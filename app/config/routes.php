<?php
/**
 * Route Configuration
 */
use Exo\Route;

Route::add('example/defined', array(
    'controller' => 'Example\Application',
    'method' => 'definedMethod'
));

Route::add('example/method', function($request, $response){
    $response->content .= '<p>Test</p>'
});

Route::add('example/:arg', array(
    'controller' => 'Example\Application'
    'method' => ':arg'
))

Route::add('fallback', array(
    'controller' => 'Example\Application'
    'method' => ':arg'
))

Route::add('', array(
    'controller' => 'Example\Fallback\Application',
    'method' => 
));
