<?php
/**
 * Test Model
 */
namespace Test;
class Model extends \ORM\Model
{
    public static $table = 'testmodel';

    /**
     * @type string
     */
    public $string;

    /**
     * @type number
     */
    public $number;

    /**
     * @type integer
     */
    public $integer;

    /**
     * unique string
     * @unique
     * @type string
     */
    public $uniqueString;

    /**
     * unique number
     * @unique
     * @type number
     */
    public $uniqueNumber;
}
