<?php
/**
 * Exoskeleton Framework
 */

// include framework
include('exo/skeleton.php');

// load the framework
Exo::setup();
Exo::execute();
Exo::teardown();
